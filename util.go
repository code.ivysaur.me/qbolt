package main

import (
	"unsafe"
	
	"github.com/mappu/miqt/qt"
)

// ReverseSlice reverses a slice.
// @ref https://stackoverflow.com/a/28058324
func ReverseSlice[S ~[]E, E any](s S)  {
    for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
        s[i], s[j] = s[j], s[i]
    }
}

func MakeQByteArray(s string) *qt.QByteArray {
	return qt.NewQByteArray7(s, len(s))
}

func FromQByteArray(qba *qt.QByteArray) string {
	var rawData []byte = unsafe.Slice((*byte)(qba.Data()), qba.Length())
	return string(rawData) // copy
}
