module code.ivysaur.me/qbolt

go 1.23

require (
	github.com/mappu/miqt v0.5.0
	go.etcd.io/bbolt v1.3.11
)

require golang.org/x/sys v0.4.0 // indirect
