
VERSION := 1.0.3
GOFLAGS_L := -ldflags='-s -w -X main.Version=v$(VERSION)' -buildvcs=false -gcflags='-trimpath=$(CURDIR)' -asmflags='-trimpath=$(CURDIR)'
GOFLAGS_W := -ldflags='-s -w -X main.Version=v$(VERSION) -H windowsgui' -buildvcs=false --tags=windowsqtstatic -gcflags='-trimpath=$(CURDIR)' -asmflags='-trimpath=$(CURDIR)'
SHELL := /bin/bash
# Allow overriding DOCKER with e.g. sudo docker
DOCKER := docker
MIQT_UIC := miqt-uic
MIQT_RCC := miqt-rcc
GO_WINRES := go-winres
SOURCES := $(wildcard *.go *.ui *.qrc) resources.go mainwindow_ui.go itemwindow_ui.go rsrc_windows_amd64.syso

.PHONY: all
all: build/qbolt build/qbolt.exe
	
.PHONY: dist
dist: build/qbolt-${VERSION}-windows-x86_64.zip build/qbolt-${VERSION}-debian12-x86_64.tar.xz

.PHONY: clean
clean:
	rm -f qbolt || true
	rm -rf build || true
	rm -f windows-manifest.json || true
	
# Generated files
	
resources.rcc resources.go: resources.qrc
	$(MIQT_RCC) resources.qrc
	
mainwindow_ui.go: mainwindow.ui
	$(MIQT_UIC) -InFile mainwindow.ui -OutFile mainwindow_ui.go

itemwindow_ui.go: itemwindow.ui
	$(MIQT_UIC) -InFile itemwindow.ui -OutFile itemwindow_ui.go
	
windows-manifest.json: windows-manifest.template.json Makefile
	cat windows-manifest.template.json | sed -re 's_%VERSION%_$(VERSION)_' > windows-manifest.json
	
rsrc_windows_amd64.syso: windows-manifest.json
	$(GO_WINRES) make --in windows-manifest.json
	rm rsrc_windows_386.syso || true # we do not build x86_32
	
# Linux release
	
build/qbolt: $(SOURCES)
	go build $(GOFLAGS_L) -o build/qbolt
	upx build/qbolt

build/qbolt-${VERSION}-debian12-x86_64.tar.xz: build/qbolt
	XZ_OPTS=-9e tar caf build/qbolt-${VERSION}-debian12-x86_64.tar.xz -C build qbolt --owner=0 --group=0
	
# Windows release (docker)

build/qbolt.exe: $(SOURCES)
	( $(DOCKER) image ls | fgrep qbolt-win64-cross ) || ( cd docker && $(DOCKER) build -t qbolt-win64-cross:latest -f win64-cross.Dockerfile . )
	$(DOCKER) run --rm -v $(CURDIR):/qbolt -w /qbolt qbolt-win64-cross:latest /bin/sh -c "go build $(GOFLAGS_W) -o build/qbolt.exe"
	upx --force build/qbolt.exe

build/qbolt-${VERSION}-windows-x86_64.zip: build/qbolt.exe
	zip -9 -j build/qbolt-${VERSION}-windows-x86_64.zip build/qbolt.exe
