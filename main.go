package main

import (
	"os"

	"github.com/mappu/miqt/qt"
)

var Version string = "v0.0.0-devel"

func main() {

	_ = qt.NewQApplication(os.Args)

	qt.QGuiApplication_SetApplicationDisplayName("QBolt")
	qt.QGuiApplication_SetWindowIcon(qt.NewQIcon4(":/rsrc/database_lightning.png"))

	w := NewMainWindow()
	w.ui.MainWindow.Show()

	qt.QApplication_Exec()
}
