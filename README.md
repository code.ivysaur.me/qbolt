# qbolt

A graphical database manager for BoltDB.

QBolt allows you to graphically view and edit the content of Bolt databases.

Written in Golang (Qt)

## Features

- Open existing database or create new database
- Option to open database as readonly for concurrent use
- Create, list, edit and delete keys and buckets (including nested buckets)
- Safe for use with arbitrary binary key/bucket names (new ones created in UTF-8)
- View database and bucket statistics
- 100% Bolt compatibility via the real codebase
- Tested working on both Windows and Linux

## License

Source code content of `qbolt-x.x.x-src.tar.gz` is released under the ISC license.
BoltDB is released under the MIT license.
The Windows binary is released under LGPL-3+ owing to the static copy of Qt.

## See also

- BoltDB https://github.com/boltdb/bolt

## Changelog

2024-10-05 1.0.3
- Port from hybrid Go/C++ to now using [MIQT](https://github.com/mappu/miqt)
- Switch Windows build to win64
- Rebuild artefacts with miqt v0.5.0, etcd-io/bbolt v1.3.11, go 1.19 (deb12), go 1.23 (win64)
- [⬇ Download here](https://git.ivysaur.me/code.ivysaur.me/qbolt/releases/tag/v1.0.3)

2020-04-12 1.0.2
- Rebuild artefacts with etcd-io/bbolt v1.3.5, go 1.15, Qt 5.15, and new GCC versions
- Switch from hg to Git
- Use Go modules
- Add support for building Windows binary in Docker
- [⬇ Download here](https://git.ivysaur.me/code.ivysaur.me/qbolt/releases/tag/v1.0.2)

2017-06-19 1.0.1
- Feature: Option to open database as read-only
- Fix an issue with support for bucket names and keys not surviving UTF-8 roundtrips (now binary-clean)
- Fix an issue with crashing when deleting a bucket other than the selected one
- Fix a cosmetic issue with application icon on Windows
- [⬇ Download here](https://git.ivysaur.me/code.ivysaur.me/qbolt/releases/tag/v1.0.1)

2017-05-21 1.0.0
- Initial public release
- The project consists of two parts; a C binding (CGo) for the embeddable Bolt database engine, and a graphical interface built in C++/Qt that links to it.
- [⬇ Download here](https://git.ivysaur.me/code.ivysaur.me/qbolt/releases/tag/v1.0.0)
