package main

import (
	"testing"
)

func TestQByteArray(t *testing.T) {
	foo := "the \x00 quick \x01 brown \x02 fox \x03 jumps \x04 over \x05 the \x06 lazy \x07 dog"

	qb := MakeQByteArray(foo)

	out := FromQByteArray(qb)

	if foo != out {
		t.Errorf("Expected equal")
	}
}
