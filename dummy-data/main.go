package main

import (
	"math/rand"
	"os"

	bolt "go.etcd.io/bbolt"
)

func random_name() string {
	ret := make([]byte, 12)
	rand.Read(ret)
	return string(ret)
	//return fmt.Sprintf("%08x-%08x-%08x", rand.Int63(), rand.Int63(), rand.Int63())
}

func fill_bucket(tx *bolt.Tx, bucket *bolt.Bucket) error {
	// fill with some basic items
	for i := 0; i < 30; i += 1 {
		err := bucket.Put([]byte(random_name()), []byte("SAMPLE CONTENT "+random_name()))
		if err != nil {
			return err
		}
	}

	// 1/20 (5%) chance of recursion
	if rand.Intn(100) >= 95 {

		for i := 0; i < 5; i += 1 {

			child, err := bucket.CreateBucket([]byte(random_name()))
			if err != nil {
				return err
			}

			err = fill_bucket(tx, child)
			if err != nil {
				return err
			}

		}

	}

	return nil
}

func main() {
	db, err := bolt.Open("sample.db", 0644, bolt.DefaultOptions)
	if err != nil {
		panic(err)
	}

	err = db.Update(func(tx *bolt.Tx) error {
		// top-level buckets
		for i := 0; i < 50; i += 1 {
			bucketName := random_name()
			bucket, err := tx.CreateBucket([]byte(bucketName))
			if err != nil {
				return err
			}

			err = fill_bucket(tx, bucket)
			if err != nil {
				return err
			}
		}

		return nil
	})

	if err != nil {
		panic(err)
	}

	err = db.Close()
	if err != nil {
		panic(err)
	}

	os.Exit(0)
}
